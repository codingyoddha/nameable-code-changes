# nameable-code-changes

We present the link to the implementations and dataset




# Dataset

Public dataset on mined nameable code changes. 

Download the dump from here


https://www.dropbox.com/sh/ig4oi9444qzn49v/AABYRE45rvfmNLcoqMqc3NXga?dl=0


# Implementation

Link to the implementation repositories

### PyDriller + Infrastructure Extension
https://gitlab.com/codingyoddha/repo-driller


### FinerGit Extension
https://gitlab.com/codingyoddha/finger-git


### CodeShovel Extension 

The webservice repository: https://gitlab.com/codingyoddha/codeshovel-webservice-gcp

Restart when crashes hack: https://gitlab.com/codingyoddha/codeshovel-service-restart

CodeShovel changes to forked repo: https://gitlab.com/codingyoddha/codeshovel







